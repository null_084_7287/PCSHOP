//app.js
var util=require("/utils/util.js");
App({
  onLaunch: function () {
    // 展示本地存储能力
    // var logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', logs)
    var that=this;
    var storage_data=wx.getStorageSync('global_data');
    if(storage_data)
    {
      this.globalData=storage_data;
      this.globalData.host="https://shop.e-notice.cn/index.php/Admin";
      this.globalData.domain="https://shop.e-notice.cn";
    }
  },
  onShow:function()
  {
    var that=this;
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
          wx.request({
            url: that.globalData.host+"/"+"Service"+"/"+"get_openid",
            header: {
              'content-type': 'application/x-www-form-urlencoded',
            },
            data:
            {
              code:res.code,
            },
            method:'Post',
            dataType:'json',
            success:function(res)
            {
              console.log(res);
              var r=res;
              if(r.data.code==1)
              {
                that.globalData.openid=r.data.data;
              }
              else
              {
                console.log('获取openid失败，错误信息:'+r.data.msg);
              }
            }
          })
      }
    })
  },
  save_data:function()
  {
    wx.setStorageSync('global_data', this.globalData);
  },
  globalData: {
    psaler:'0',
    userInfo: null,
    domain:'https://shop.e-notice.cn',
    host:"https://shop.e-notice.cn/index.php/Admin",
  }
})