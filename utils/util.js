var app=getApp();
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
function ftime (number, format) {
  let time = new Date(number)
  let newArr = []
  let formatArr = ['Y', 'M', 'D', 'h', 'm', 's']
  newArr.push(time.getFullYear())
  newArr.push(formatNumber(time.getMonth() + 1))
  newArr.push(formatNumber(time.getDate()))
  
  newArr.push(formatNumber(time.getHours()))
  newArr.push(formatNumber(time.getMinutes()))
  newArr.push(formatNumber(time.getSeconds()))
  
  for (let i in newArr) {
   format = format.replace(formatArr[i], newArr[i])
  }
  return format;
 }
//提示框并返回
function show_model_and_back(msg)
{
  wx.showModal({
    title:"提示",
    content:msg,
    success:function()
    {
      wx.navigateBack({
        delta: 1,
      });
    }
  })
}
function show_model(msg)
{
  wx.showModal({
    title:"提示",
    content:msg,
    success:function()
    {
    }
  })
}
function alert(msg,func1,func2)
{
  wx.showModal({
    title:"提示",
    content:msg,
    success:function(res)
    {
      if(res.confirm)
      {
        func1();
      }
      else
      {
        func2();
      }
    }
  })
}
//rpx2px
function rpx2px(r,systeminfo)
{
  return parseInt((r/750)*systeminfo.windowWidth);
}
//比较版本号
function compare_version(curV,reqV){
  if(curV && reqV){
     //将两个版本号拆成数字
     var arr1 = curV.split('.'),
         arr2 = reqV.split('.');
     var minLength=Math.min(arr1.length,arr2.length),
         position=0,
         diff=0;
     //依次比较版本号每一位大小，当对比得出结果后跳出循环（后文有简单介绍）
     while(position<minLength && ((diff=parseInt(arr1[position])-parseInt(arr2[position]))==0)){
         position++;
     }
     diff=(diff!=0)?diff:(arr1.length-arr2.length);
     //若curV大于reqV，则返回true
     return diff>0;
  }else{
     //输入为空
     console.log("版本号不能为空");
     return false;
  }
}
//申请订阅消息权限
function apply_template(systeminfo,ids_template,func)
{
  var times=0;
  var max_count=1;
  if((systeminfo.system.toLowerCase().indexOf('ios')>=0&&compare_version(systeminfo.version,'7.0.6')>=0)||(systeminfo.system.toLowerCase().indexOf('android')>=0&&compare_version(systeminfo.version,'7.0.7')>=0))
  {
    max_count=3;
  }
  var list_ids_template=new Array();
  for(var i=0;i<ids_template.length;i+=max_count)
  {
    var end=i+max_count;
    if(end>=ids_template.length)
    {
      end=ids_template.length;
    }
    console.log(ids_template);
    console.log(i+":"+end);
    console.log(ids_template.slice(i,end));
    list_ids_template.push(ids_template.slice(i,end));
  }
  for(var i=0;i<list_ids_template.length;i++)
  {
    wx.requestSubscribeMessage({
      tmplIds: list_ids_template[i],
      success:function(res)
      {
        console.log(res);
      },
      fail:function(res)
      {
        console.log(res);
      },
      complete:function()
      {
        times++;
        console.log('请求订阅消息次数:'+times);
        if(times==list_ids_template.length)
        {
          func();
        }
      }
    })
  }
}
//根据link对象进行跳转
function click_link(link)
{
  console.log(link);
  if(link.url!='')
  {
    var url=link.url;
    if(link.param!='')
    {
      url+=("?"+link.param+"="+link.val);
    }
    wx.navigateTo({
      url: url,
    })
  }
}
function format_param(urlSearch)
{
  let ret = {}
  let regParam = /([^&=]+)=([\w\W]*?)(&|$|#)/g
  if (urlSearch) {
    var strParam = urlSearch;
    var result
    while ((result = regParam.exec(strParam)) != null) {
      ret[result[1]] = result[2]
    }
  }
  return ret
}
function navigateTo(d)
{
  var url=d.url;
  var indexUrl ="pages/ui/ui" // 小程序入口页面
  var page = getCurrentPages(); // 获取到小程序的页面栈
  var delta = -1; 
  if(page.length>1){
          
    for(var i=0;i<page.length;i++){
      console.log(url+":"+("/"+page[i].route));
      if(url==("/"+page[i].route)){ //入口页面的索引i
        delta = page.length - 1 -i   // 算出要回退几步回到 入口页面
        break
      }
    }
  }
  if(delta==-1){
    console.log('前进');
    if(getCurrentPages().length>=9)
    {
      wx.redirectTo({
        url: d.url,
      })
    }
    else{
      wx.navigateTo({
        url: d.url,
      })
    }
  }else{
    console.log('后退:'+delta);
    wx.navigateBack({
      delta: delta
    })
  }
}

module.exports = {
  formatTime: formatTime,
  ftime:ftime,
  show_model_and_back: show_model_and_back,
  show_model:show_model,
  rpx2px: rpx2px,
  alert:alert,
  compare_version:compare_version,
  apply_template:apply_template,
  click_link:click_link,
  navigateTo:navigateTo,
  format_param:format_param,
}
