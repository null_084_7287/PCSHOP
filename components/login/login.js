// components/login/login.js
const app=getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    domain:'',

  },

  /**
   * 组件的初始数据
   */
  data: {
    need_auth:false,
    domain:app.globalData.domain,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    cancel:function()
    {
      wx.navigateBack({
        delta: 1,
      });
    },
    confirm:function(res)
    {
      var thiss=this;
      console.log(res.detail.userInfo);
      app.globalData.userinfo=res.detail.userInfo;
      this.triggerEvent("confirm",
        {
          code:1,
          msg:'success'
        },
        {}
      );
    },
    cancel_login:function()
    {
      this.triggerEvent("cancel_login",
        {
        },
        {}
      );
    },
    get_phone_number:function(e)
    {
      var encryptedData=e.detail.encryptedData;
      var iv=e.detail.iv;
      if(encryptedData==null||iv==null)
      {
        util.alert(
          '获取手机号失败，请重试',
          function(e)
          {},
          function(e)
          {}
        );
      }
      else
      {
        this.triggerEvent("getphonenumber",
          {
            code:1,
            msg:'success',
            encryptedData:e.detail.encryptedData,
            iv:e.detail.iv
          },
          {}
        );
      }
    },
    getuserinfo:function()
    {
      var thiss=this;
      wx.getSetting({
        success: res => {
          if (res.authSetting['scope.userInfo']) {
            // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
            wx.getUserInfo({
              success: res => {
                // 可以将 res 发送给后台解码出 unionId
                this.globalData.userInfo = res.userInfo
  
                // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                // 所以此处加入 callback 以防止这种情况
                if (this.userInfoReadyCallback) {
                  this.userInfoReadyCallback(res)
                }
              }
            })
          }
          else
          {
          }
        }
      })
    }
  }
})
