const util = require("../../utils/util");

// components/ui_mofang/ui_mofang.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        moudle:null,
        domain:null,
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        click:function(e)
        {
            var thiss=this;
            var index=e.currentTarget.dataset.index;
            var link=thiss.properties.moudle.p.rows_img[index].link;
            util.click_link(link);
        }
    }
})
