const util = require("../../utils/util");

// components/ui_title/ui_title.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        moudle:null,
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        goto_more:function()
        {
            console.log('more');
            util.click_link(this.properties.moudle.p.desclink);
        },
    }
})
