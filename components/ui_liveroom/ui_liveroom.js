// components/ui_liveroom/ui_liveroom.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        moudle:null,
        domain:null,
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        goto_liveroom:function(e)
        {
            var thiss=this;
            var index=e.currentTarget.dataset.index;
            var row_liveroom=thiss.properties.moudle.p.rows_liveroom[index];
            wx.navigateTo({
                url: 'plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id='+row_liveroom.id,
            })
        },
    }
})
