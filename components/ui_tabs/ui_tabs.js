const util = require("../../utils/util");

// components/ui_tabs/ui_tabs.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        tabs:null,
        domain:null,
        index:
        {
            type:Number,
            value:0,
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        click_tab:function(e)
        {
            var links=new Array();
            links.push("/pages/ui/ui");
            links.push("/pages/category/category");
            links.push("/pages/cart/cart");
            links.push("/pages/usercenter/usercenter");
            links.push("/pages/coupon/coupon");
            links.push("/pages/sign/sign");
            links.push("/pages/collection/collection");
            links.push("/pages/activity_group/activity_group");
            links.push("/pages/activity_flash/activity_flash");
            links.push("/pages/activity_bargain/activity_bargain");
            var thiss=this;
            var index=e.currentTarget.dataset.index;
            if(index!=thiss.properties.index)
            {
                var tab=thiss.properties.tabs.tabs[index];
                var url=links[tab.link_type];
                if(url==null)
                {
                  url=tab.link;
                }
                console.log(url);
                wx.navigateTo({
                  url: url,
                })
            }
        },
    }
})
