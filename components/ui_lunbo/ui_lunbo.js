const util = require("../../utils/util");

// components/ui_lunbo/ui_lunbo.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        moudle:null,
        domain:null,
        current:
        {
            type:Number,
            value:0,
        },
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        change_index:function(current,source)
        {
            console.log(current);
            var thiss=this;
            thiss.setData(
                {
                    current:current.detail.current,
                }
            );
            console.log(thiss.data.current);
        },
        click:function(e)
        {
            var thiss=this;
            console.log(e);
            var index=e.target.dataset.index;
            util.click_link(thiss.properties.moudle.p.rows_img[index].link);
        }
    }
})
