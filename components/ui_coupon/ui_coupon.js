// components/ui_coupon/ui_coupon.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        moudle:null,
        domain:null,
    },

    /**
     * 组件的初始数据
     */
    data: {
        array5:[1,2,3,4,5],
        array6:[1,2,3,4,5,6]
    },

    /**
     * 组件的方法列表
     */
    methods: {
        click:function(e)
        {
            var thiss=this;
            console.log(e);
            var index=e.currentTarget.dataset.index;
            thiss.triggerEvent(
                'click_coupon',
                {
                    data:thiss.data.moudle.p.rows_coupon[index],
                },
                {}
            );
        },
    }
})
