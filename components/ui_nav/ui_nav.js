// components/ui_nav/ui_nav.js
const util = require("../../utils/util");
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        moudle:null,
        domain:{
            type:String,
            value:'',
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        page_count:0,
        pages:[],
        index_page:0,
    },
    lifetimes: {
        attached: function() {
          // 在组件实例进入页面节点树时执行
          console.log('attached');
          this.ini_page_count();
        },
        detached: function() {
          // 在组件实例被从页面节点树移除时执行
        },
      },
      // 以下是旧式的定义方式，可以保持对 <2.2.3 版本基础库的兼容
      attached: function() {
        // 在组件实例进入页面节点树时执行
          console.log('attached');
          this.ini_page_count();
      },
      detached: function() {
        // 在组件实例被从页面节点树移除时执行
      },
    /**
     * 组件的方法列表
     */
    methods: {
        ini_page_count:function()
        {
            var thiss=this;
            thiss.setData(
                {
                    page_count:thiss.properties.moudle.p.rows_data.length%(thiss.properties.moudle.p.count_row*thiss.properties.moudle.p.per_row)>0?(1+parseInt(thiss.properties.moudle.p.rows_data.length/(thiss.properties.moudle.p.count_row*thiss.properties.moudle.p.per_row))):parseInt(thiss.properties.moudle.p.rows_data.length/(thiss.properties.moudle.p.count_row*thiss.properties.moudle.p.per_row)),
                }
            );
            var pages=[];
            for(var i=0;i<thiss.data.page_count;i++)
            {
                pages.push(i);
            }
            thiss.setData(
                {
                    pages:pages,
                }
            );
            console.log(thiss.data.page_count);
        },
        change_index:function(current,source)
        {
            console.log(current);
            var thiss=this;
            thiss.setData(
                {
                    index_page:current.detail.current,
                }
            );
        },
        click:function(e)
        {
            var thiss=this;
            var index=e.currentTarget.dataset.index;
            console.log(index);
            var link=thiss.properties.moudle.p.rows_data[index].link;
            util.click_link(link);
        },
    }
})
