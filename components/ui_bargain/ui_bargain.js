// components/ui_bargain/ui_bargain.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        moudle:null,
        domain:null,
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        click:function(e)
        {
            var thiss=this;
            var index=e.currentTarget.dataset.index;
            console.log(index);
            wx.navigateTo({
                url: '/pages/product_bargain/product_bargain?id='+thiss.properties.moudle.p.rows_data[index].id,
            })
        }
    }
})
