// components/ui_product/ui_product.js
var app=getApp();
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        moudle:null,
        domain:{
            type:String,
            value:'',
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
      domain:app.globalData.domain,
    },

    /**
     * 组件的方法列表
     */
    methods: {
        click:function(e)
        {
            var thiss=this;
            var index=e.currentTarget.dataset.index;
            console.log(index);
            //商品
            wx.navigateTo({
              url: '/pages/product/product?id='+thiss.properties.moudle.p.rows_data[index].id,
            })
        }
    }
})
