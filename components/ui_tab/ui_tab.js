// components/ui_tab/ui_tab.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        moudle:null,
        domain:{
            type:String,
            value:'',
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        index_tab:0,
    },

    /**
     * 组件的方法列表
     */
    methods: {
        click_tab:function(e)
        {
            var thiss=this;
            var index=e.currentTarget.dataset.index;
            console.log(index);
            thiss.setData(
                {
                    index_tab:parseInt(index),
                }
            );
        },
        click:function(e)
        {
            var thiss=this;
            var index=e.currentTarget.dataset.index;
            console.log(index);
            if(thiss.properties.moudle.p.rows_tab[thiss.data.index_tab].type=='1')
            {
                //商品
                wx.navigateTo({
                  url: '/pages/product/product?id='+thiss.properties.moudle.p.rows_tab[thiss.data.index_tab].rows_data[index].id,
                })
            }
            else if(thiss.properties.moudle.p.rows_tab[thiss.data.index_tab].type=='2')
            {
                //秒杀
                wx.navigateTo({
                    url: '/pages/product_flash/product_flash?id='+thiss.properties.moudle.p.rows_tab[thiss.data.index_tab].rows_data[index].id,
                })
            }
            else if(thiss.properties.moudle.p.rows_tab[thiss.data.index_tab].type=='3')
            {
                //拼团
                wx.navigateTo({
                    url: '/pages/product_group/product_group?id='+thiss.properties.moudle.p.rows_tab[thiss.data.index_tab].rows_data[index].id,
                })
            }
            else if(thiss.properties.moudle.p.rows_tab[thiss.data.index_tab].type=='4')
            {
                //砍价
                wx.navigateTo({
                    url: '/pages/product_bargain/product_bargain?id='+thiss.properties.moudle.p.rows_tab[thiss.data.index_tab].rows_data[index].id,
                })
            }
        }
    }
})
