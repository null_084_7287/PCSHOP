// pages/product/product.js
var util=require("../../utils/util.js");
var pcapi=require("../../utils/pcapi.js");
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    domain:'',
    id:0,
    show_login:false,
    show_panel:false,
    number:1,
    row_flash:null,
    row_flashdetail:null,
    state:0,//0：未开始 1：进行中 2：结束
    time_hour:0,
    time_minute:0,
    time_second:0,
    config:null,
  },
  get_config:function()
  {
    var thiss=this;
    pcapi.get_config("",
      function(res)
      {
        thiss.setData(
          {
            config:res.data.data,
          }
        );
      }
    );
  },
  add_cart:function(e)
  {
    var thiss=this;
    util.navigateTo(
      {
        url:'/pages/product/product?id='+thiss.data.row_flash.product_id,
      }
    );
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thiss=this;
    thiss.get_config();
    var id=options.id;
    console.log(id);
    this.setData(
      {
        domain:app.globalData.domain,
        id:id,
      }
    );
    if(app.globalData.row_member==null)
    {

      this.setData(
        {
          show_login:true,
        }
      );
    }
    setInterval(
      function()
      {
        thiss.check_time();
      },
      1000
    )
  },
  //获取用户信息权限
  do_login:function(phone)
  {
    var thiss=this;
    this.setData(
      {
        show_login:false,
      }
    );
    wx.login({
      success:function(res)
      {
        console.log(res.code);
        pcapi.api(
          'Service',
          'get_code',
          {
            code:res.code,
            phone:phone,
            nickname:"",
            img:"",
            psaler:app.globalData.psaler,
          },
          function(res)
          {
            console.log(res);
            if(res.data.code==1)
            {
              app.globalData.row_member=res.data.data;
              app.save_data();
              thiss.setData(
                {
                  row_member:app.globalData.row_member,
                }
              );
              thiss.get_flash_detail();
            }
            else{
              util.show_model_and_back(res.data.msg);
            }
          }
        );
      },
      fail:function(res)
      {
        util.show_model_and_back('登录失败');
      }
    })
  },
  
  
  get_phone_number:function(r)
  {
    var thiss=this;
    console.log(r);
    if(r.detail.code==1)
    {
      //检查session_key是否失效
      wx.checkSession({
        success: (res) => {
          console.log(res);
          //解密数据
          pcapi.api(
            'Service',
            'wechat_decrypt',
            {
              openid:app.globalData.openid,
              encrypteddata:r.detail.encryptedData,
              iv:r.detail.iv
            },
            function(res)
            {
              console.log(res);
              if(res.data.code==0)
              {
                util.alert(
                  res.data.msg,
                  function(e)
                  {},
                  function(e)
                  {}
                );
              }
              else{
                var phone=res.data.data.purePhoneNumber;
                //进行注册
                thiss.do_login(phone);
              }
            }
          );
        },
        fail:(res)=>
        {
          util.alert(
            '微信登录超时，请重试',
            function(e)
            {
              // thiss.get_openid();
            },
            function(e)
            {
              // thiss.get_openid();
            }
          );
        }
      })
    }
  },
  //获取用户信息权限
  // do_login:function(res)
  // {
  //   var thiss=this;
  //   this.setData(
  //     {
  //       show_login:false,
  //     }
  //   );
  //   console.log(res);
  //   wx.login({
  //     success:function(res)
  //     {
  //       console.log(res.code);
  //       pcapi.do_login(res.code,
  //           function(res)
  //           {
  //             console.log(res);
  //             if(res.data.code==1)
  //             {
  //               app.globalData.row_member=res.data.data;
  //               app.save_data();
  //               thiss.get_flash_detail();
  //             }
  //             else{
  //               util.show_model_and_back(res.data.msg);
  //             }
  //           }
  //         );
  //     },
  //     fail:function(res)
  //     {
  //       util.show_model_and_back('登录失败');
  //     }
  //   })
  // },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.getSystemInfo({
      success: (result) => {
        // console.log(result);
        this.data.systeminfo=result;
      },
    });
    this.data.menu_rect=wx.getMenuButtonBoundingClientRect();
    console.log(this.data);
    this.setData(
      this.data
    );
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.get_flash_detail();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  get_flash_detail:function()
  {
    //
    var thiss=this;
   
    pcapi.get_flash_detail(
      thiss.data.id,
      app.globalData.row_member.id,
      function(res)
      {
        if(res.data.code==1)
        {
          thiss.setData(
            {
              row_flash:res.data.data,
            }
          );
          thiss.ini_row_flashdetail();
        }
        else
        {
          util.show_model_and_back(res.data.msg);
        }
      }
    );
  },
  ini_row_flashdetail:function()
  {
    var thiss=this;
  
    if(thiss.data.row_flash.row_product.single_spec=="1")
    {
      var row_flashdetail=thiss.data.row_flash.rows_flashdetail[0];
      if(parseInt(row_flashdetail.disabled)==0)
      {
        thiss.setData(
          {
            row_flashdetail:row_flashdetail,
          }
        );
      }
    }
    else
    {
      //多规格需要遍历数据
      for(var i=0;i<thiss.data.row_flash.rows_flashdetail.length;i++)
      {
        var row_flashdetail=thiss.data.row_flash.rows_flashdetail[i];
        if(parseInt(row_flashdetail.disabled)==0)
        {
          thiss.setData(
            {
              row_flashdetail:row_flashdetail,
            }
          );
          i=thiss.data.row_flash.row_product.rows_productspec.length;
        }
      }
    }
  },
 check_time:function()
  {
    var thiss=this;
    if(thiss.data.row_flash!=null)
    {
      var now=new Date().getTime()/1000;
      var stime=new Date(thiss.data.row_flash.stime).getTime()/1000;
      var etime=new Date(thiss.data.row_flash.etime).getTime()/1000;
      if(now<stime)
      {
        thiss.data.state=0;
        //计算
        thiss.data.time_hour=parseInt((stime-now)/3600);
        thiss.data.time_minute=parseInt((stime-now)%3600/60)>9?parseInt((stime-now)%3600/60):"0"+parseInt((stime-now)%3600/60);
        thiss.data.time_second=parseInt((stime-now)%60)>9?parseInt((stime-now)%60):"0"+parseInt((stime-now)%60);
      }
      else if(now>=stime&&now<etime)
      {
        thiss.data.state=1;
        thiss.data.time_hour=parseInt((etime-now)/3600);
        thiss.data.time_minute=parseInt((etime-now)%3600/60)>9?parseInt((etime-now)%3600/60):"0"+parseInt((etime-now)%3600/60);
        thiss.data.time_second=parseInt((etime-now)%60)>9?parseInt((etime-now)%60):"0"+parseInt((etime-now)%60);
      }
      else
      {
        thiss.data.state=2;
      }
      thiss.setData(
        {
          state:thiss.data.state,
          time_hour:thiss.data.time_hour,
          time_minute:thiss.data.time_minute,
          time_second:thiss.data.time_second,
        }
      );
    }
  },
  buy:function()
  {
    var thiss=this;
    if(thiss.data.state!=1)
    {
      return;
    }
    if(!thiss.data.show_panel)
    {
      thiss.data.show_panel=true
      thiss.setData(
        {
          show_panel:thiss.data.show_panel,
        }
      );
    }
    else
    {
      //
      if(thiss.data.row_flashdetail==null)
      {
        wx.showToast({
          title: '请选择规格',
        });
      }
      else
      {
        if(thiss.data.number<1)
        {
          wx.showToast({
            title: '请选择数量',
          });
        }
        else
        {
          var product_id=thiss.data.row_flash.product_id;
          var category_id=thiss.data.row_flash.row_product.category_id;
          var productspec_id=thiss.data.row_flashdetail.row_productspec.id;
          var price=thiss.data.row_flashdetail.flash_price;
          var number=thiss.data.number;
          var row_orderlist=new Object();
          row_orderlist.product_id=product_id;
          row_orderlist.category_id=category_id;
          row_orderlist.productspec_id=productspec_id;
          row_orderlist.price=price;
          row_orderlist.number=number;
          var rows_orderlist=new Array();
          rows_orderlist.push(row_orderlist);
          app.globalData.rows_orderlist=rows_orderlist;
          wx.navigateTo({
            url: '/pages/bill_build/bill_build?type=2&detail_id='+thiss.data.id,
          })
        }
      }
    }
  },
  do_nothing:function()
  {},
  select_spec:function(e)
  {
    var thiss=this;
    var spec_id=e.currentTarget.dataset.id;
    var rows_flashdetail=thiss.data.row_flash.rows_flashdetail;
    //获取specclass_id
    console.log(spec_id);
    console.log(rows_flashdetail);
    for(var i=0;i<rows_flashdetail.length;i++)
    {
        if(rows_flashdetail[i].productspec_id==spec_id)
        {
          if(rows_flashdetail[i].disabled==0)
          {
            thiss.setData(
              {
                row_flashdetail:rows_flashdetail[i]
              }
            );
          }
        }
    }
  },
  add:function()
  {
    //判断row_productspec是否存在，没有的话需要选择规格
    var thiss=this;
    if(thiss.data.row_flashdetail==null)
    {
      wx.showToast({
        title: '请选择规格',
      });
    }
    else{
      var new_number=thiss.data.number+1;
      var stock=parseInt(thiss.data.row_flashdetail.row_productspec.stock);
      if(parseInt(thiss.data.row_flashdetail.flash_limit)>0&&parseInt(thiss.data.row_flashdetail.flash_limit)<stock)
      {
        stock=parseInt(thiss.data.row_flashdetail.flash_limit);
      }
      if(new_number<=stock)
      {
        thiss.setData(
          {
            number:new_number,
          }
        );
      }
      else
      {
        thiss.setData(
          {
            number:stock,
          }
        );
      }
    }
  },
  subduce:function()
  {
    //判断row_productspec是否存在，没有的话需要选择规格
    var thiss=this;
    if(thiss.data.row_flashdetail==null)
    {
      wx.showToast({
        title: '请选择规格',
      });
    }
    else{
      var new_number=thiss.data.number-1;
      if(new_number<0)
      {
        new_number=0;
      }
      var stock=parseInt(thiss.data.row_flashdetail.row_productspec.stock);
      if(new_number<=stock)
      {
        thiss.setData(
          {
            number:new_number,
          }
        );
      }
      else
      {
        thiss.setData(
          {
            number:stock,
          }
        );
      }
    }
  },
  close_panel:function(res)
  {
    this.setData(
      {
        show_panel:false,
      }
    );
  },
})