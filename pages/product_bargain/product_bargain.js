// pages/product_bargain/product_bargain.js
var util=require("../../utils/util.js");
var pcapi=require("../../utils/pcapi.js");
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    domain:'',
    row_member:null,
    id:0,//bargain_id
    row_bargain:null,
    bargainlist_id:0,//如果大于0，代表给别人砍价
    row_bargainlist:null,
    show_login:false,
    time2end:"",
    config:null,
    rows_bargainlist:null,//所有参与某一bargainlist的rows_bargainlist
    bargain_money:null
  },
  get_config:function()
  {
    var thiss=this;
    pcapi.get_config("",
      function(res)
      {
        thiss.setData(
          {
            config:res.data.data,
          }
        );
        console.log(thiss.data.config);
      }
    );
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thiss=this;
    thiss.setData(
      {
        domain:app.globalData.domain,
      }
    );
    thiss.get_config();
    //窗口信息
    wx.getSystemInfo({
      success: (result) => {
        console.log(result);
        this.data.systeminfo=result;
      },
    });
    var scene=options.scene;
    console.log(scene);
    if(scene!=null)
    {
      var ps=util.format_param(decodeURIComponent(scene));
      console.log(ps);
      if(ps.id!=null&&ps.bargainlist_id!="")
      {
        thiss.setData(
          {
            id:ps.id,
            bargainlist_id:ps.bargainlist_id,
          }
        );
      }
    }
    this.data.menu_rect=wx.getMenuButtonBoundingClientRect();
    var id=options.id;
    console.log(id);
    this.setData(
      {
        id:id,
      }
    );
    if(options.bargainlist_id!=null)
    {
      this.setData(
        {
          bargainlist_id:parseInt(options.bargainlist_id),
        }
      );
    }
    setInterval(
      function()
      {
        thiss.check_time_to_end();
      },
      1000
    )
  },
  check_time_to_end:function()
  {
    var thiss=this;
    if(thiss.data.row_bargain!=null)
    {
      var edate=new Date(thiss.data.row_bargain.edate).getTime()/1000;
      if(thiss.data.row_bargainlist!=null)
      {
        edate=new Date(thiss.data.row_bargainlist.edate).getTime()/1000;
      }
      var now=new Date().getTime()/1000;
      var day=parseInt((edate-now)/86400);
      var hour=parseInt((edate-now)%86400/3600);
      var minute=parseInt((edate-now)%3600/60);
      var second=parseInt((edate-now)%3600%60);
      var time2end="";
      if(day>0)
      {
        time2end+=(day+"天");
      }
      if(hour>0)
      {
        time2end+=(day+"时");
      }
      if(minute>0)
      {
        time2end+=(minute+"分");
      }
      if(second>0)
      {
        time2end+=(second+"秒");
      }
      thiss.setData(
        {
          time2end:time2end,
        }
      );
    }
  },
  //获取用户信息权限
  // do_login:function(res)
  // {
  //   var thiss=this;
  //   this.setData(
  //     {
  //       show_login:false,
  //     }
  //   );
  //   console.log(res);
  //   wx.login({
  //     success:function(res)
  //     {
  //       console.log(res.code);
  //       pcapi.do_login(res.code,
  //           function(res)
  //           {
  //             console.log(res);
  //             if(res.data.code==1)
  //             {
  //               app.globalData.row_member=res.data.data;
  //               app.save_data();
  //               thiss.setData(
  //                 {
  //                   row_member:app.globalData.row_member,
  //                 }
  //               );
  //               //重新获取商品详情
  //               thiss.get_bargain_detail();
  //             }
  //             else{
  //               util.show_model_and_back(res.data.msg);
  //             }
  //           }
  //         );
  //     },
  //     fail:function(res)
  //     {
  //       util.show_model_and_back('登录失败');
  //     }
  //   })
  // },
  //获取用户信息权限
  do_login:function(phone)
  {
    var thiss=this;
    this.setData(
      {
        show_login:false,
      }
    );
    wx.login({
      success:function(res)
      {
        console.log(res.code);
        pcapi.api(
          'Service',
          'get_code',
          {
            code:res.code,
            phone:phone,
            nickname:"",
            img:"",
            psaler:app.globalData.psaler,
          },
          function(res)
          {
            console.log(res);
            if(res.data.code==1)
            {
              app.globalData.row_member=res.data.data;
              app.save_data();
              thiss.setData(
                {
                  row_member:app.globalData.row_member,
                }
              );
              //重新获取商品详情
              if(thiss.data.bargainlist_id==null)
              {
                thiss.get_bargain_detail();
              }
              else{
                thiss.get_bargainlist();
              }
            }
            else{
              util.show_model_and_back(res.data.msg);
            }
          }
        );
      },
      fail:function(res)
      {
        util.show_model_and_back('登录失败');
      }
    })
  },
  get_phone_number:function(r)
  {
    var thiss=this;
    console.log(r);
    if(r.detail.code==1)
    {
      //检查session_key是否失效
      wx.checkSession({
        success: (res) => {
          console.log(res);
          //解密数据
          pcapi.api(
            'Service',
            'wechat_decrypt',
            {
              openid:app.globalData.openid,
              encrypteddata:r.detail.encryptedData,
              iv:r.detail.iv
            },
            function(res)
            {
              console.log(res);
              if(res.data.code==0)
              {
                util.alert(
                  res.data.msg,
                  function(e)
                  {},
                  function(e)
                  {}
                );
              }
              else{
                var phone=res.data.data.purePhoneNumber;
                //进行注册
                thiss.do_login(phone);
              }
            }
          );
        },
        fail:(res)=>
        {
          util.alert(
            '微信登录超时，请重试',
            function(e)
            {
              // thiss.get_openid();
            },
            function(e)
            {
              // thiss.get_openid();
            }
          );
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var thiss=this;
    if(app.globalData.row_member==null)
    {
      this.setData(
        {
          show_login:true,
        }
      );
    }
    else
    {
      this.setData(
        {
          row_member:app.globalData.row_member,
        }
      );
      //获取砍价详情
      if(thiss.data.bargainlist_id>0)
      {
        //获取
        thiss.get_bargainlist();
      }
      else{
        //获取row_baragin
        thiss.get_bargain_detail();
      }
      // thiss.get_bargain_detail();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var path='/pages/product_bargain/product_bargain?id='+this.data.id;
    if(this.data.row_bargain.now_row_bargainlist!=null)
    {
      path+=('&bargainlist_id='+this.data.row_bargain.now_row_bargainlist.id);
    }
    var obj=
    {
      title:"帮我砍价",
      path:path,
    };
    return obj;
  },


  get_bargain_detail:function()
  {
    var thiss=this;
    pcapi.get_bargain_detail(
      thiss.data.id,
      null,
      null,
      function(res)
      {
        if(res.data.code==1)
        {
          thiss.setData(
            {
              row_bargain:res.data.data,
            }
          );
          thiss.check_bargainlist();
        }
        else
        {
          util.show_model_and_back(res.data.msg);
        }
      }
    );
  },
  get_bargainlist:function(){
    var thiss=this;
    var condition={
      id:thiss.data.bargainlist_id,
    };
    pcapi.api(  
      'Service',
      'flist',
      {
        table:'bargainlist',
        condition:JSON.stringify(condition)
      },
      function(res){
        if(res.data.code==1)
        {
          if(res.data.data.length>0)
          {
            thiss.setData(
              {
                row_bargainlist:res.data.data[0],
                row_bargain:res.data.data[0].row_bargain,
              }
            );
          }
          else{
            util.show_model_and_back("获取砍价活动明细失败");
          }
        }
        else
        {
          util.show_model_and_back(res.data.msg);
        }
    })
  },
  //获取当前用户是否存在未结束的砍价活动
  check_bargainlist:function()
  {
    var thiss=this;
    var condition={
      bargain_id:thiss.data.id,
      _string:'id=bargainlist_id',
      member_id:thiss.data.row_member.id,
      state:'0'
    };
    pcapi.api(
      'Service',
      'flist',
      {
        table:'bargainlist',
        condition:JSON.stringify(condition),
      },
      function(res)
      {
        if(res.data.code==1)
        {
          if(res.data.data.length>0)
          {
            var bargainlist_id=res.data.data[0].id;
            wx.redirectTo({
              url: '/pages/product_bargain/product_bargain?id='+thiss.data.id+"&bargainlist_id="+bargainlist_id,
            });
          }
        }
        else{
          //
          util.show_model_and_back('检查未完成砍价活动明细失败');
        }
      }
    );
  },
  create_bargainlist:function()
  {
    var thiss=this;
    pcapi.create_bargainlist(
      app.globalData.row_member.id,
      thiss.data.id,
      function(res)
      {
        if(res.data.code==1)
        {
          if(res.data.ids_template!=null)
          {
            util.apply_template(thiss.data.systeminfo,res.data.ids_template,
              function()
              {
                console.log("请求权限完成");
                thiss.setData(
                  {
                    bargainlist_id:res.data.bargainlist_id,
                  }
                );
                thiss.get_bargainlist();
              }
            );
          }
          else
          {
            thiss.setData(
              {
                bargainlist_id:res.data.bargainlist_id,
              }
            );
            thiss.get_bargainlist();
          }
        }
        else
        {
          util.show_model_and_back(res.data.msg);
        }
      }
    );
  },
  add_bargainlist:function()
  {
    var thiss=this;
    pcapi.add_bargainlist(
      app.globalData.row_member.id,
      thiss.data.row_bargain.now_row_bargainlist.id,
      function(res)
      {
        if(res.data.code==1)
        {
          if(res.data.ids_template!=null)
          {
            util.apply_template(thiss.data.systeminfo,res.data.ids_template,
              function()
              {
                console.log("请求权限完成");
                thiss.get_bargain_detail();
              }
            );
          }
          else
          {
            thiss.get_bargain_detail();
          }
        }
        else
        {
          util.show_model(res.data.msg);
        }
      }
    );
  },
  get_bargain_poster:function()
  {
    var thiss=this;
    wx.navigateTo({
      url: '/pages/poster_bargain/poster_bargain?id='+thiss.data.id+"&bargainlist_id="+thiss.data.bargainlist_id,
    })
  },
  build_bill:function()
  {
    //
    var thiss=this;
    var product_id=thiss.data.row_bargain.row_product.id;
    var category_id=thiss.data.row_bargain.row_product.category_id;
    var productspec_id=thiss.data.row_bargain.productspec_id;
    var price=thiss.data.row_bargain.price;
    var number=1;
    var row_orderlist=new Object();
    row_orderlist.product_id=product_id;
    row_orderlist.category_id=category_id;
    row_orderlist.productspec_id=productspec_id;
    row_orderlist.price=price;
    row_orderlist.number=number;
    var rows_orderlist=new Array();
    rows_orderlist.push(row_orderlist);
    app.globalData.rows_orderlist=rows_orderlist;
    wx.navigateTo({
      url: '/pages/bill_build/bill_build?type=3&detail_id='+thiss.data.row_bargain.now_row_bargainlist.id,
    })
  }
})