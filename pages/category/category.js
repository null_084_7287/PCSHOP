// pages/category/category.js
var util=require("../../utils/util.js");
var pcapi=require("../../utils/pcapi.js");
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    domain:'',
    config:null,
    rows_category:[],
    selected_index:0,
    scroll_right_id:"",
    right_empty_height:0,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thiss=this;
    thiss.get_config();
    thiss.setData(
      {
        domain:app.globalData.domain,
      }
    );
  },
  get_config:function()
  {
    var thiss=this;
    pcapi.get_config("",
      function(res)
      {
        thiss.setData(
          {
            config:res.data.data,
          }
        );
      }
    );
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.getSystemInfo({
      success: (result) => {
        console.log(result);
        this.data.systeminfo=result;
      },
    });
    this.data.menu_rect=wx.getMenuButtonBoundingClientRect();
    this.setData(
      this.data
    );
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.get_category();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  get_category:function()
  {
    var thiss=this;
    pcapi.get_category(
      function(res)
      {
        thiss.setData(
          {
            rows_category:res.data.data,
          }
        );

            thiss.ini_height();
       
        }
    );
    
  },
 
  ini_height:function()
  {
      var thiss=this;
      wx.createSelectorQuery().selectAll('.brand').boundingClientRect().exec(
        function(rows_rect)
        {
          console.log(rows_rect);
          for(var i=0;i<rows_rect[0].length;i++)
          {
            var rect=rows_rect[0][i];
            var height=rect.height;
            thiss.data.rows_category[i].height=height;
          }
        //计算右侧下方空白区域的高度 
        var last= rows_rect[0].pop();
        var last_height=last.height;
        console.log(last_height);
        var right_height=thiss.data.systeminfo.windowHeight-thiss.data.menu_rect.height-10; 
        console.log(right_height+":"+last_height); 
        thiss.setData( 
          { 
            right_empty_height:right_height-last_height, 
          } 
        ); 
        console.log(thiss.data.rows_category); 
        }
      );
  },
  goto_product_list:function(event)
  {
    console.log(event);
    var id=event.currentTarget.dataset.id;
    console.log(id);
    wx.navigateTo({
      url: '/pages/product_list/product_list?id='+id,
    })
  },
  click_left:function(e)
  {
    var id=e.currentTarget.dataset.id;
    console.log("category_"+id);
    this.setData(
      {
        scroll_right_id:"category_"+id,
      }
    );
  },
  ini_selected:function(top)
  {
    console.log(top);
      var thiss=this;
      var index=-1;
      var all_height=0;
      for(var i=0;i<thiss.data.rows_category.length;i++)
      {
          var row_category=thiss.data.rows_category[i];
    
          var height=row_category.height;
          if(top>=all_height)
          {
              index=i;
          }
          else
          {
              i=thiss.data.rows_category.length;
          }
          all_height=all_height+height;
      }
      console.log(all_height);
      if(thiss.data.selected_index!=index)
      {
          thiss.setData(
              {
                selected_index:index,
              }
          );
      }
      console.log(thiss.data.selected_index);
  },
  scroll_right:function(e)
  {
    var thiss=this;
    var top=e.detail.scrollTop;
    var thiss=this;
    thiss.ini_selected(top);
  },
  change_keyword:function(e)
  {
    console.log(e);
    wx.navigateTo({
      url: '/pages/product_list/product_list?name='+e.detail.value,
    })
  },
})