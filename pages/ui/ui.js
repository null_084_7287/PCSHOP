// pages/ui/ui.js
var util=require("../../utils/util.js");
var pcapi=require("../../utils/pcapi.js");
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        domain:'',
        page_id:-1,
        page:null,
        show_login:false,
        index_tab:-1,
        tabs:null,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var thiss=this;
        thiss.setData(
            {
                domain:app.globalData.domain,
            }
        );
        if(options.page_id!=null)
        {
            thiss.setData(
                {
                    page_id:options.page_id,
                }
            );
            thiss.get_page_detail();
        }
        else
        {
            thiss.get_home();
        }
    },
    get_home:function()
    {
        var thiss=this;
        pcapi.api(
            "Service",
            'get_home_page',
            {
            },
            function(res)
            {
                console.log(res);
                if(res.data.code==1)
                {
                    thiss.setData(
                        {
                            page:res.data.data,
                        }
                    );
                }
            }
        );
        thiss.get_tabs();
    },
    get_tabs:function()
    {
        var pages = getCurrentPages(); //获取加载的页面
        var currentPage = pages[pages.length-1]; //获取当前页面的对象
        var url = currentPage.route; //当前页面url
        var thiss=this;
        pcapi.api(
            "Service",
            'get_tabs',
            {
                url:url,
            },
            function(res)
            {
                console.log(res);
                if(res.data.code==1)
                {
                    thiss.setData(
                        {
                            tabs:res.data.data,
                            index_tab:res.data.index,
                        }
                    );
                }
                else{
                    util.show_model(res.data.msg);
                }
            }
        );
    },
    get_page_detail:function()
    {
        var thiss=this;
        pcapi.api(
            "Service",
            'get_page_detail',
            {
                page_id:thiss.data.page_id,
            },
            function(res)
            {
                console.log(res);
                if(res.data.code==1)
                {
                    thiss.setData(
                        {
                            page:res.data.data,
                        }
                    );
                }
            }
        );
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    cancel_login:function(e)
    {
        this.setData(
            {
                show_login:false,
            }
        );
    },
    click_coupon:function(e)
    {
        var thiss=this;
        console.log(e);
        if(app.globalData.row_member==null)
        {
            console.log('未登录');
          this.setData(
            {
              show_login:true,
            }
          );
        }
        else{
            thiss.add_couponlist(e.detail.data.id);
        }
    },
    add_couponlist:function(coupon_id)
    {
      var thiss=this;
      pcapi.add_couponlist(
        app.globalData.row_member.id,
        coupon_id,
        function(res)
        {
          if(res.data.code==1)
          {
            util.show_model(res.data.msg);
          }
          else
          {
            util.show_model(res.data.msg);
          }
        }
      );
    },
    get_phone_number:function(r)
    {
        var thiss=this;
        thiss.setData(
            {
                show_login:false,
            }
        );
        pcapi.get_phone_number(r,
            function()
            {
                //success
                thiss.setData(
                    {
                        row_member:app.globalData.row_member,
                    }
                );
                util.show_model("登录成功");
            },
            function(msg)
            {
                util.show_model(msg);
            }
        );
    }
})