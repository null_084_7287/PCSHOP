// pages/product_group/product_group.js
var util=require("../../utils/util.js");
var pcapi=require("../../utils/pcapi.js");
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    domain:'',
    id:0,
    grouplist_id:0,//如果开团的话=0，如果参加别人的团的话>0
    show_login:false,
    show_panel:false,
    number:1,
    row_group:null,
    row_groupdetail:null,
    config:null,
  },
  get_config:function()
  {
    var thiss=this;
    pcapi.get_config("",
      function(res)
      {
        thiss.setData(
          {
            config:res.data.data,
          }
        );
      }
    );
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thiss=this;
    thiss.get_config();
    console.log(options);
    this.setData(
      {
        domain:app.globalData.domain,
        id:options.id,
      }
    );
    if(app.globalData.row_member==null)
    {
      this.setData(
        {
          show_login:true,
        }
      );
    }
    setInterval(
      function()
      {
        thiss.ini_time_remain();
      },
      1000
    )
  },
  //获取用户信息权限
  do_login:function(phone)
  {
    var thiss=this;
    this.setData(
      {
        show_login:false,
      }
    );
    wx.login({
      success:function(res)
      {
        console.log(res.code);
        pcapi.api(
          'Service',
          'get_code',
          {
            code:res.code,
            phone:phone,
            nickname:"",
            img:"",
            psaler:app.globalData.psaler,
          },
          function(res)
          {
            console.log(res);
            if(res.data.code==1)
            {
              app.globalData.row_member=res.data.data;
              app.save_data();
              thiss.setData(
                {
                  row_member:app.globalData.row_member,
                }
              );
              thiss.get_group_detail();
            }
            else{
              util.show_model_and_back(res.data.msg);
            }
          }
        );
      },
      fail:function(res)
      {
        util.show_model_and_back('登录失败');
      }
    })
  },
  get_phone_number:function(r)
  {
    var thiss=this;
    console.log(r);
    if(r.detail.code==1)
    {
      //检查session_key是否失效
      wx.checkSession({
        success: (res) => {
          console.log(res);
          //解密数据
          pcapi.api(
            'Service',
            'wechat_decrypt',
            {
              openid:app.globalData.openid,
              encrypteddata:r.detail.encryptedData,
              iv:r.detail.iv
            },
            function(res)
            {
              console.log(res);
              if(res.data.code==0)
              {
                util.alert(
                  res.data.msg,
                  function(e)
                  {},
                  function(e)
                  {}
                );
              }
              else{
                var phone=res.data.data.purePhoneNumber;
                //进行注册
                thiss.do_login(phone);
              }
            }
          );
        },
        fail:(res)=>
        {
          util.alert(
            '微信登录超时，请重试',
            function(e)
            {
              // thiss.get_openid();
            },
            function(e)
            {
              // thiss.get_openid();
            }
          );
        }
      })
    }
  },
  //获取用户信息权限
  // do_login:function(res)
  // {
  //   this.setData(
  //     {
  //       show_login:false,
  //     }
  //   );
  //   console.log(res);
  //   wx.login({
  //     success:function(res)
  //     {
  //       console.log(res.code);
  //       pcapi.do_login(res.code,
  //           function(res)
  //           {
  //             console.log(res);
  //             if(res.data.code==1)
  //             {
  //               app.globalData.row_member=res.data.data;
  //               app.save_data();
  //               thiss.get_group_detail();
  //             }
  //             else{
  //               util.show_model_and_back(res.data.msg);
  //             }
  //           }
  //         );
  //     },
  //     fail:function(res)
  //     {
  //       util.show_model_and_back('登录失败');
  //     }
  //   })
  // },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

    wx.getSystemInfo({
      success: (result) => {
        console.log(result);
        this.data.systeminfo=result;
      },
    });
    this.data.menu_rect=wx.getMenuButtonBoundingClientRect();
    this.setData(
      this.data
    );
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.get_group_detail();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  get_group_detail:function()
  {
    //
    var thiss=this;
    pcapi.get_group_detail(
      thiss.data.id,
      app.globalData.row_member.id,
      function(res)
      {
        console.log('row_group');
        console.log(res.data.data);
        if(res.data.code==1)
        {
          thiss.setData(
            {
              row_group:res.data.data,
            }
          );
          thiss.ini_row_groupdetail();
        }
        else
        {
          util.show_model_and_back(res.data.msg);
        }
      }
    );
  },
  ini_time_remain:function()
  {
    var thiss=this;
    if(thiss.data.row_group!=null)
    {
      for(var i=0;i<thiss.data.row_group.rows_grouplist.length;i++)
      {
        var row_grouplist=thiss.data.row_group.rows_grouplist[i];
        var edate=row_grouplist.edate;
        var etime=parseInt(new Date(edate).getTime()/1000-new Date().getTime()/1000);
        var remain_hour=parseInt(etime/3600);
        var remain_minute=parseInt(etime%3600/60);
        var remain_second=parseInt(etime%60);
        var time_remain=remain_hour+":"+remain_minute+":"+remain_second;
        thiss.data.row_group.rows_grouplist[i].etime=etime;
        thiss.data.row_group.rows_grouplist[i].time_remain=time_remain;
        thiss.setData(
          {
            row_group:thiss.data.row_group,
          }
        );
        console.log(thiss.data.row_group);
      }
    }
  },
  ini_row_groupdetail:function()
  {
    var thiss=this;
    if(thiss.data.row_group.row_product.single_spec==1)
    {
      var row_groupdetail=thiss.data.row_group.rows_groupdetail[0];
      if(parseInt(row_groupdetail.disabled)==0)
      {
        thiss.setData(
          {
            row_groupdetail:row_groupdetail,
          }
        );
      }
    }
    else
    {
      //多规格需要遍历数据
      for(var i=0;i<thiss.data.row_group.rows_groupdetail.length;i++)
      {
        var row_groupdetail=thiss.data.row_group.rows_groupdetail[i];
        if(parseInt(row_groupdetail.disabled)==0)
        {
          thiss.setData(
            {
              row_groupdetail:row_groupdetail,
            }
          );
          i=thiss.data.row_group.row_product.rows_productspec.length;
        }
      }
    }
  },

  do_nothing:function()
  {},
  select_spec:function(e)
  {
    var thiss=this;
    var spec_id=e.currentTarget.dataset.id;
    var rows_groupdetail=thiss.data.row_group.rows_groupdetail;
    //获取specclass_id
    for(var i=0;i<rows_groupdetail.length;i++)
    {
        if(rows_groupdetail[i].productspec_id==spec_id&&parseInt(rows_groupdetail[i].disabled)==0)
        {
          thiss.setData(
            {
              row_groupdetail:rows_groupdetail[i]
            }
          );
          console.log(thiss.data.row_groupdetail);
        }
    }
    console.log(this.data.row_groupdetail);
  },
  add:function()
  {
    //判断row_productspec是否存在，没有的话需要选择规格
    var thiss=this;
    if(thiss.data.row_groupdetail==null)
    {
      wx.showToast({
        title: '请选择规格',
      });
    }
    else{
      var new_number=thiss.data.number+1;
      var stock=parseInt(thiss.data.row_groupdetail.row_productspec.stock);
      if(parseInt(thiss.data.row_groupdetail.group_limit)>0&&parseInt(thiss.data.row_groupdetail.group_limit)<stock)
      {
        stock=parseInt(thiss.data.row_groupdetail.group_limit);
      }
      if(new_number<=stock)
      {
        thiss.setData(
          {
            number:new_number,
          }
        );
      }
      else
      {
        thiss.setData(
          {
            number:stock,
          }
        );
      }
    }
  },
  subduce:function()
  {
    //判断row_productspec是否存在，没有的话需要选择规格
    var thiss=this;
    if(thiss.data.row_productspec==null)
    {
      wx.showToast({
        title: '请选择规格',
      });
    }
    else{
      var new_number=thiss.data.number-1;
      if(new_number<0)
      {
        new_number=0;
      }
      var stock=parseInt(thiss.data.row_groupdetail.row_productspec.stock);
      if(new_number<=stock)
      {
        thiss.setData(
          {
            number:new_number,
          }
        );
      }
      else
      {
        thiss.setData(
          {
            number:stock,
          }
        );
      }
    }
  },
  buy:function(e)
  {
    var grouplist_id=e.currentTarget.dataset.id;
    var thiss=this;
    if(!thiss.data.show_panel)
    {
      thiss.data.show_panel=true
      thiss.setData(
        {
          show_panel:thiss.data.show_panel,
          grouplist_id:parseInt(grouplist_id),
        }
      );
    }
    else
    {
      //
      if(thiss.data.row_groupdetail==null)
      {
        wx.showToast({
          title: '请选择规格',
        });
      }
      else
      {
        if(thiss.data.number<1)
        {
          wx.showToast({
            title: '请选择数量',
          });
        }
        else
        {
          var product_id=thiss.data.row_group.product_id;
          var category_id=thiss.data.row_group.row_product.category_id;
          var productspec_id=thiss.data.row_groupdetail.productspec_id;
          var price=thiss.data.row_groupdetail.group_price;
          var number=thiss.data.number;
          var row_orderlist=new Object();
          row_orderlist.product_id=product_id;
          row_orderlist.category_id=category_id;
          row_orderlist.productspec_id=productspec_id;
          row_orderlist.price=price;
          row_orderlist.number=number;
          var rows_orderlist=new Array();
          rows_orderlist.push(row_orderlist);
          app.globalData.rows_orderlist=rows_orderlist;
          wx.navigateTo({
            url: '/pages/bill_build/bill_build?type=1&detail_id='+thiss.data.id+"&&grouplist_id="+thiss.data.grouplist_id,
          })
        }
      }
    }
  },
  close_panel:function(res)
  {
    this.setData(
      {
        show_panel:false,
      }
    );
  },
  to_product:function(){
    wx.navigateTo({
      url: '/pages/product/product',
    })
  }
})