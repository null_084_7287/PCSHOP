// pages/usercenter/usercenter.js
var util=require("../../utils/util.js");
var pcapi=require("../../utils/pcapi.js");
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show_login:false,
    row_member:null,
    domain:'',
    tabs:null,
    index_tab:-1,
    config:null,
  },
  get_config:function()
  {
    var thiss=this;
    pcapi.get_config("",
      function(res)
      {
        thiss.setData(
          {
            config:res.data.data,
          }
        );
      }
    );
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thiss=this;
    thiss.get_config();
    thiss.data.domain=app.globalData.domain;
    thiss.setData({
      domain:thiss.data.domain,
    });
  },
  //获取用户信息权限
  do_login:function(phone)
  {
    var thiss=this;
    this.setData(
      {
        show_login:false,
      }
    );
    wx.login({
      success:function(res)
      {
        console.log(res.code);
        pcapi.api(
          'Service',
          'get_code',
          {
            code:res.code,
            phone:phone,
            nickname:"",
            img:"",
            psaler:app.globalData.psaler,
          },
          function(res)
          {
            console.log(res);
            if(res.data.code==1)
            {
              app.globalData.row_member=res.data.data;
              app.save_data();
              thiss.setData(
                {
                  row_member:app.globalData.row_member,
                }
              );
            }
            else{
              util.show_model_and_back(res.data.msg);
            }
          }
        );
      },
      fail:function(res)
      {
        util.show_model_and_back('登录失败');
      }
    })
  },
  get_phone_number:function(r)
  {
    var thiss=this;
    console.log(r);
    if(r.detail.code==1)
    {
      //检查session_key是否失效
      wx.checkSession({
        success: (res) => {
          console.log(res);
          //解密数据
          pcapi.api(
            'Service',
            'wechat_decrypt',
            {
              openid:app.globalData.openid,
              encrypteddata:r.detail.encryptedData,
              iv:r.detail.iv
            },
            function(res)
            {
              console.log(res);
              if(res.data.code==0)
              {
                util.alert(
                  res.data.msg,
                  function(e)
                  {},
                  function(e)
                  {}
                );
              }
              else{
                var phone=res.data.data.purePhoneNumber;
                //进行注册
                thiss.do_login(phone);
              }
            }
          );
        },
        fail:(res)=>
        {
          util.alert(
            '微信登录超时，请重试',
            function(e)
            {
              // thiss.get_openid();
            },
            function(e)
            {
              // thiss.get_openid();
            }
          );
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var thiss=this; 
    console.log("show");
    console.log(app.globalData);
    if(app.globalData.row_member==null)
    {
      this.setData(
        {
          show_login:true,
        }
      );
    }
    else{
      //刷新用户信息
      this.refresh_member();
    }
    thiss.get_tabs();
  },
  get_tabs:function()
  {
      var pages = getCurrentPages(); //获取加载的页面
      var currentPage = pages[pages.length-1]; //获取当前页面的对象
      var url = currentPage.route; //当前页面url
      var thiss=this;
      pcapi.api(
          "Service",
          'get_tabs',
          {
              url:url,
          },
          function(res)
          {
              console.log(res);
              if(res.data.code==1)
              {
                  thiss.setData(
                      {
                          tabs:res.data.data,
                          index_tab:res.data.index,
                      }
                  );
              }
              else{
                  util.show_model(res.data.msg);
              }
          }
      );
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  refresh_member:function()
  {
    var thiss=this;
    pcapi.refresh_member(
      function(res)
      {
        if(res.data.code==1)
        {
          console.log(res);
          thiss.setData(
            {
              row_member:res.data.data,
            }
          );
        }
      }
    );
  },
  goto_orderlist:function(e)
  {
    var state=e.currentTarget.dataset.state;
    var thiss=this;
    wx.navigateTo({
      url: '/pages/orderlist/orderlist?state='+state,
    })
  },
  goto_refundlist:function()
  {
    wx.navigateTo({
      url: '/pages/refundlist/refundlist',
    })
  },
  goto_spread:function()
  {
    wx.navigateTo({
      url: '/pages/spread/spread',
    })
  },
  goto_my_money:function()
  {
    wx.navigateTo({
      url: '/pages/my_money/my_money',
    })
  },
  goto_my_integral:function()
  {
    wx.navigateTo({
      url: '/pages/my_integral/my_integral',
    })
  },
  goto_collection:function()
  {
    wx.navigateTo({
      url: '/pages/collection/collection',
    })
  },
  goto_coupon:function()
  {
    wx.navigateTo({
      url: '/pages/coupon/coupon',
    })
  },
  goto_pick_address:function()
  {
    wx.navigateTo({
      url: '/pages/pick_address/pick_address',
    })
  },
  goto_my_bargainlist:function()
  {
    wx.navigateTo({
      url: '/pages/my_bargainlist/my_bargainlist',
    })
  },
  goto_my_phone:function()
  {
    wx.navigateTo({
      url: '/pages/my_phone/my_phone',
    })
  },
  do_scan:function()
  {
    var thiss=this;
    wx.scanCode({
      onlyFromCamera: true,
      success:function(res)
      {
        console.log(res);
        var qrcode=res.result;
        thiss.get_order_qrcode(qrcode);
      }
    })
  },
  get_order_qrcode:function(orderno)
  {
    var thiss=this;
    pcapi.get_order_qrcode(
      {
        member_id:app.globalData.row_member.id,
        orderno:orderno,
      },
      function(res)
      {
        util.show_model(res.data.msg);
      }
    );
  },
})